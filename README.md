# TCP port check

Looks for open ports over an IP Address.

## Usage

Basic usage
> .\main.ps1 -Address \<IP Address\>


You can also specify optional parameters:

- Start: Port to begin the search (default is 1).
> .\main.ps1 -Address \<IP Address> -Start \<Port>
- Limit: Port to end the search (default is 65535).
> .\main.ps1 -Address \<IP Address> -Limit \<Port>
- Output: Path to print the result (default is in current folder).
> .\main.ps1 -Address \<IP Address> -Output path\to\my.file

Full example:
> .\main.ps1 -Address 35.180.139.74 -Start 80 -Limit 90 -Output C:\Users\UserName\Desktop\output.log