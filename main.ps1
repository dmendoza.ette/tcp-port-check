Param(
  [Parameter(Mandatory = $true,
    ValueFromPipeline = $true)]
  [String]
  $Address = $null,
  [int] $Start = 1,
  [int] $Limit = 65535,
  [String] $Output = ".\ports.txt"
)

Out-Null

if ($Address -eq $null) {
  Write-Host "Missing `"-Address`" parameter";
  Return;
}

$pattern = "^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
$good = $Address -match $pattern;

if (-Not $good) {
  Write-Host "Bad IP Address";
  Return;
}

$ports = "Open ports in `"$Address`":";

for ($i = $Start; $i -le $Limit; $i++) {
  # Write-Output(Test-NetConnection -ComputerName portquiz.net -Port 80 | {});
  $ports = $ports + "`n$(Test-NetConnection -ComputerName $Address -Port $i | Where-Object { $_.TcpTestSucceeded } | ForEach-Object { return "$($_.RemotePort)" })";
}

$ports | Out-File -FilePath $Output
Write-Host "DONE!";